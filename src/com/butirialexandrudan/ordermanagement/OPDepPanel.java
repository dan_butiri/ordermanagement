package com.butirialexandrudan.ordermanagement;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public class OPDepPanel extends JPanel {

	private GridBagConstraints gbc;
	private MyTable myTable;
	private JTable table;
	private JScrollPane scrollPane;
	private DefaultListModel<String> dlm;
	private JList<String> detaliiList;
	private JScrollPane scrollPaneOrder;
	private JButton btnOnorat = new JButton("Onorat!");

	/**
	 * Create the panel.
	 * 
	 * @param oPDep
	 *            lista cu comenzile inregistrate
	 */
	public OPDepPanel(OPDep oPDep) {
		gbc = new GridBagConstraints();
		myTable = new MyTable(Order.class, AddThis.class,
				new ArrayList<Object>(oPDep.toArrayList()));
		table = new JTable(myTable);
		scrollPane = new JScrollPane(table);
		dlm = new DefaultListModel<String>();
		detaliiList = new JList<String>(dlm);
		scrollPaneOrder = new JScrollPane(detaliiList);

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.setLayout(new GridBagLayout());
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.2;
		gbc.weighty = 0.2;

		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(scrollPane, gbc);

		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		this.add(scrollPaneOrder, gbc);

		gbc.weighty = 0.0;
		gbc.weightx = 0.0;
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(btnOnorat, gbc);
	}

	/**
	 * Returneaza butonul btnOnorat (pentru stergerea comenzilor onorate din
	 * tabel si lista).
	 * 
	 * @return butonul btnOnorat
	 */
	public JButton getBtnOnorat() {
		return btnOnorat;
	}

	/**
	 * Returneaza obiectul care retine elementele care sunt puse in tabel
	 * (myTable).
	 * 
	 * @return obiectul care retine elementele care sunt puse in tabel
	 */
	public MyTable getMyTable() {
		return myTable;
	}

	/**
	 * Returneaza tabelul de pe panel (table).
	 * 
	 * @return tabelul de pe panel
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * Returneaza lista in care sunt afisate detalii despre comanda.
	 * 
	 * @return lista in care sunt afisate detaliile despre comanda
	 */
	public DefaultListModel<String> getDlm() {
		return dlm;
	}
}
