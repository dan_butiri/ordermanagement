package com.butirialexandrudan.ordermanagement;

import java.util.Comparator;

public class TreeElement<T> implements Comparable<T> {
	private Comparator<T> comparator;
	private TreeElement<T> childLeft;
	private TreeElement<T> childRight;
	private TreeElement<T> oldMan;
	private T data;

	/**
	 * Creaza obiectul si seteaza modul de comparare a obiectelor.
	 * 
	 * @param newComparator
	 *            noul comparator
	 */
	public TreeElement(Comparator<T> newComparator) {
		comparator = newComparator;
		childLeft = null;
		childRight = null;
		oldMan = null;
		data = null;
	}

	/**
	 * Creaza obiectul si seteaza modul de comparare a obiectelor si informatia
	 * stocata.
	 * 
	 * @param newComparator
	 *            noul comparator
	 * @param newData
	 *            noua informatie pentru obiect
	 */
	public TreeElement(T newData, Comparator<T> newComparator) {
		comparator = newComparator;
		childLeft = null;
		childRight = null;
		oldMan = null;
		data = newData;
	}

	/**
	 * Compara obiectul care apeleaza metoda cu un obiect trimis ca parametru.
	 * 
	 * @param compareT
	 *            obiectul cu care se compara
	 * @return rezultatul comparari (=0 - egalitate, <0 - obiectul "compareT" e
	 *         mai mare)
	 */
	public int compareTo(T compareT) {
		return comparator.compare(this.data, compareT);
	}

	/**
	 * Returneaza fiul stanga al acestui nod.
	 * 
	 * @return fiul stanga al acestui nod
	 */
	public TreeElement<T> getChildLeft() {
		return childLeft;
	}

	/**
	 * Seteaza fiul stanga al acestui nod.
	 * 
	 * @param childLeft
	 *            fiul stanga
	 */
	public void setChildLeft(TreeElement<T> childLeft) {
		this.childLeft = childLeft;
	}

	/**
	 * Returneaza fiul dreapta al acestui nod.
	 * 
	 * @return fiul dreapta al acestui nod
	 */
	public TreeElement<T> getChildRight() {
		return childRight;
	}

	/**
	 * Seteaza fiul dreapta al acestui nod.
	 * 
	 * @param childRight
	 *            fiul dreapta
	 */
	public void setChildRight(TreeElement<T> childRight) {
		this.childRight = childRight;
	}

	/**
	 * Returneaza parintele acestui nod.
	 * 
	 * @return parintele nodului
	 */
	public TreeElement<T> getOldMan() {
		return oldMan;
	}

	/**
	 * Seteaza parintele acestui nod.
	 * 
	 * @param oldMan
	 *            parintele nodului
	 */
	public void setOldMan(TreeElement<T> oldMan) {
		this.oldMan = oldMan;
	}

	/**
	 * Returneaza informatia stocata in nod.
	 * 
	 * @return informatia stocata in nod
	 */
	public T getData() {
		return data;
	}

	/**
	 * Seteaza informatia din nod.
	 * 
	 * @param data
	 *            informatia din nod
	 */
	public void setData(T data) {
		this.data = data;
	}
}
