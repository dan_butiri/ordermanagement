package com.butirialexandrudan.ordermanagement;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MyTable extends AbstractTableModel {
	private ArrayList<String> header;
	private ArrayList<Object> table;
	private ArrayList<Boolean> editableRow;
	private ArrayList<Boolean> editableColumn;

	/**
	 * Creaza obiectul.
	 * 
	 * @param c
	 *            clasa dupa care sa se creze tabelul
	 * @param cAnno
	 *            anotatile folosite pentru marcarea campurilor care sa apara in
	 *            table
	 * @param newTable
	 *            obiectele care sa populeze tabelul.
	 */
	public MyTable(Class<?> c, Class<? extends Annotation> cAnno,
			ArrayList<Object> newTable) {
		header = new ArrayList<String>();
		table = newTable;
		editableRow = new ArrayList<Boolean>();
		editableColumn = new ArrayList<Boolean>();

		Field field[] = c.getDeclaredFields();
		for (Field f : field) {
			if (f.isAnnotationPresent(cAnno)) {
				String name = f.getName();
				String aux = name.substring(0, 1).toUpperCase()
						+ name.substring(1);
				header.add(aux);
				editableColumn.add(false);
			}
		}

		for (@SuppressWarnings("unused")
		Object o : table) {
			editableRow.add(false);
		}
	}

	/**
	 * Returneaza numarul de coloane din tabel.
	 * 
	 * @return numarul de coloane
	 */
	public int getColumnCount() {
		return header.size();
	}

	/**
	 * Returneaza numarul de linii(randuri) din tabel.
	 * 
	 * @return numarul de linii
	 */
	public int getRowCount() {
		return table.size();
	}

	/**
	 * Returneaza numele unei coloane x (capul de coloana).
	 * 
	 * @param column
	 *            coloana a carui nume dorim sa fie returnat
	 * @return numele coloanei
	 */
	public String getColumnName(int column) {
		return header.get(column);
	}

	/**
	 * Returneaza o celula din tabel de pe linia x, coloana y.
	 * 
	 * @param row
	 *            linia pe care se afla celula
	 * @param column
	 *            coloana pe care se afla celula
	 * @return celula de linia "row" si coloana "column"
	 */
	public Object getValueAt(int row, int column) {
		String aux = "get" + header.get(column);

		try {

			return table.get(row).getClass().getDeclaredMethod(aux)
					.invoke(table.get(row));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Seteaza o celula din tabel de pe linia x, coloana y.
	 * 
	 * @param row
	 *            linia pe care se afla celula
	 * @param column
	 *            coloana pe care se afla celula
	 */
	public void setValueAt(Object o, int row, int column) {
		String setAux = "set" + header.get(column);
		String getAux = "get" + header.get(column);

		try {
			Class<?> type = table.get(row).getClass().getDeclaredMethod(getAux)
					.getReturnType();

			if (o.getClass().getTypeName().toString().compareTo(type.getName()) == 0) {
				table.get(row).getClass().getDeclaredMethod(setAux, type)
						.invoke(table.get(row), o);
			} else {
				try {
					table.get(row)
							.getClass()
							.getDeclaredMethod(setAux, type)
							.invoke(table.get(row),
									Integer.parseInt((String) o));
				} catch (Exception e) {
					try {
						table.get(row)
								.getClass()
								.getDeclaredMethod(setAux, type)
								.invoke(table.get(row),
										Double.parseDouble((String) o));
					} catch (Exception e2) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.fireTableCellUpdated(row, column);
		this.fireTableStructureChanged();
	}

	/**
	 * Actualizeaza datele din tabel.
	 */
	public void revalidate() {
		this.fireTableStructureChanged();
	}

	/**
	 * Returneaza daca o celula din tabel de pe linia x, coloana y este
	 * editabila.
	 * 
	 * @param row
	 *            linia pe care se afla celula
	 * @param column
	 *            coloana pe care se afla celula
	 * @return true daca celula este editabila
	 */
	public boolean isCellEditable(int row, int column) {
		return editableRow.get(row) && editableColumn.get(column);
	}

	/**
	 * Seteaza celula din tabel de pe linia x, coloana y sa fie sau nu
	 * editabila.
	 * 
	 * @param row
	 *            linia pe care se afla celula
	 * @param column
	 *            coloana pe care se afla celula
	 * @param editable
	 *            true pentru a seta celula editablila
	 */
	public void setCellEditable(int row, int column, boolean editable) {
		this.editableRow.set(row, editable);
		this.editableColumn.set(column, editable);
	}

	/**
	 * Adauga un obiect in tabel.
	 * 
	 * @param o
	 *            obiectul adaugat
	 */
	public void addObject(Object o) {
		table.add(o);
		editableRow.add(false);

		this.fireTableDataChanged();
	}

	/**
	 * Schimba tabelul cu unul nou.
	 * 
	 * @param newTable
	 *            noul tabel
	 */
	public void addNewTable(ArrayList<Object> newTable) {
		table = newTable;
		for (@SuppressWarnings("unused")
		Object o : table) {
			editableRow.add(false);
		}
		this.fireTableDataChanged();
	}

	/**
	 * Sterge o linie din tabel.
	 * 
	 * @param row
	 *            linia care sa se stearga
	 */
	public void removeObject(int row) {
		table.remove(table.get(row));
		editableRow.remove(editableRow.get(row));
		this.fireTableDataChanged();
	}

	/**
	 * Sterge toate obiectele din tabel.
	 */
	public void removeAllObject() {
		table.removeAll(table);
		editableRow.removeAll(editableRow);
		this.fireTableDataChanged();
	}

}
