package com.butirialexandrudan.ordermanagement;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class CustomerPanel extends JPanel {

	private GridBagConstraints gbc;
	private MyTable myTable;
	private JTable table;
	private JScrollPane scrollPane;
	private JButton btnAdd = new JButton("Adauga");
	private JButton btnDelete = new JButton("Sterge");
	private JButton btnCauta = new JButton("Cauta");
	private JTextField tfCauta = new JTextField();

	/**
	 * Creaza penal-ul.
	 * 
	 * @param customerList
	 *            lista cu clienti.
	 */
	public CustomerPanel(CustomerList customerList) {
		gbc = new GridBagConstraints();
		myTable = new MyTable(Customer.class, AddThis.class,
				new ArrayList<Object>(customerList.toArrayList()));
		table = new JTable(myTable);
		scrollPane = new JScrollPane(table);

		this.setLayout(new GridBagLayout());
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.2;
		gbc.weighty = 0.2;

		gbc.gridwidth = 6;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(scrollPane, gbc);

		gbc.weighty = 0.0;
		gbc.weightx = 0.0;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(btnAdd, gbc);

		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy = 1;
		this.add(btnDelete, gbc);

		gbc.gridwidth = 1;
		gbc.gridx = 2;
		gbc.gridy = 1;
		this.add(btnCauta, gbc);

		gbc.gridwidth = 3;
		gbc.gridx = 3;
		gbc.gridy = 1;
		this.add(tfCauta, gbc);

		addEditable();
	}

	/**
	 * Returneaza obiectul care retine elementele care sunt puse in tabel
	 * (myTable).
	 * 
	 * @return obiectul care retine elementele care sunt puse in tabel
	 */
	public MyTable getMyTable() {
		return myTable;
	}

	/**
	 * Returneaza tabelul in care sunt afisati clienti (table).
	 * 
	 * @return tabelul in care sunt afisati clienti
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * Returneaza butonul care adauga clienti (btnAdd).
	 * 
	 * @return butonul de adaugare
	 */
	public JButton getBtnAdd() {
		return btnAdd;
	}

	/**
	 * Returneaza butonul care sterge clienti (btnDelete).
	 * 
	 * @return butonul de stergere
	 */
	public JButton getBtnDelete() {
		return btnDelete;
	}

	/**
	 * Returneaza butonul care cauta clienti (btnCauta).
	 * 
	 * @return butonul de stergere
	 */
	public JButton getBtnCauta() {
		return btnCauta;
	}

	/**
	 * Returneaza text field-ul in care se scrie numele clientului cautat
	 * (tfCauta).
	 * 
	 * @return text field-ul in care se scrie numele clientului cautat
	 */
	public JTextField getTfCauta() {
		return tfCauta;
	}

	private void addEditable() {
		for (int i = 0; i < myTable.getRowCount(); ++i) {
			myTable.setCellEditable(i, 1, true);
			myTable.setCellEditable(i, 2, true);
			myTable.setCellEditable(i, 3, true);
		}
	}
}
