package com.butirialexandrudan.ordermanagement;

public class Customer implements Comparable<Customer> {
	@AddThis
	private int id;
	@AddThis
	private String nume;
	@AddThis
	private int varsta;
	@AddThis
	private String adresa;

	/**
	 * Creaza obiectul.
	 */

	public Customer() {
		id = 0;
		nume = "Fara nume";
		varsta = 0;
		adresa = "Fara adresa";

	}

	/**
	 * Creaza obiectul si seteaza numele clientului.
	 * 
	 * @param nume
	 *            numele clientului
	 */

	public Customer(String nume) {
		id = 0;
		this.nume = nume;
		varsta = 0;
		adresa = "Fara adresa";
	}

	/**
	 * Returneaza id-ul clientului.
	 * 
	 * @return id-ul clientului
	 */
	public int getId() {
		return id;
	}

	/**
	 * Seteaza id-ul clientului.
	 * 
	 * @param id
	 *            id-ul clientului
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Returneaza numele clientului.
	 * 
	 * @return numele clientului
	 */
	public String getNume() {
		return nume;
	}

	/**
	 * Seteaza numele clientului.
	 * 
	 * @param nume
	 *            numele clientului
	 */
	public void setNume(String nume) {
		this.nume = nume;
	}

	/**
	 * Returneaza varsta clientului.
	 * 
	 * @return varsta clientului
	 */
	public int getVarsta() {
		return varsta;
	}

	/**
	 * Seteaza varsta clientului. Daca parametrul este negativ se seteaza la
	 * valoarea 0.
	 * 
	 * @param varsta
	 *            varsta clientului
	 */
	public void setVarsta(int varsta) {
		this.varsta = varsta > 0 ? varsta : 0;
	}

	/**
	 * Returneaza adresa clientului.
	 * 
	 * @return adresa clientului
	 */
	public String getAdresa() {
		return adresa;
	}

	/**
	 * Seteaza adresa clientului.
	 * 
	 * @param adresa
	 *            adresa clientului
	 */
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	/**
	 * Returneaza diferenta dintre id-ul clientului care apeleaza metoda si
	 * id-ul clientului trimis ca si parametru.
	 * 
	 * @param customer
	 *            clientul cu care se compara
	 * @return diferenta dintre id-ul clientului care apeleaza metoda si id-ul
	 *         clientului trimis ca si parametru
	 */
	public int compareTo(Customer customer) {
		return id - customer.getId();
	}
}
