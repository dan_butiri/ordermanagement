package com.butirialexandrudan.ordermanagement;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.butirialexandrudan.ordermanagement.Warehouse.WarehousePolicy;

public class WarehouseControl {
	private WarehousePanel managerPanel;
	private Warehouse warehouse;
	private MyTable table;

	/**
	 * Creaza obieccutl.
	 * 
	 * @param newMainFrame
	 *            fereastra principala.
	 * @param newWarehouse
	 *            lista cu produse.
	 */
	WarehouseControl(MainFrame newMainFrame, Warehouse newWarehouse) {
		this.managerPanel = newMainFrame.getMainPanel().getManagerPanel();
		this.warehouse = newWarehouse;
		this.table = newMainFrame.getMainPanel().getManagerPanel().getMyTable();

		managerPanel.getBtnNou().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg) {

				addProduct(WarehousePolicy.NEW_PRODUCT);

			}

		});

		managerPanel.getBtnCauta().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg) {

				cauta();

			}
		});

		managerPanel.getTfCauta().addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent arg) {

				if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
					cauta();
				}

			}

			public void keyReleased(KeyEvent arg) {

			}

			public void keyTyped(KeyEvent arg) {

			}

		});

		managerPanel.getBtnModifica().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg) {

				modificaProduct(WarehousePolicy.APPEND_PRODUCT);

			}
		});

		managerPanel.getBtnSterge().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg) {
				Product auxProduct = new Product();
				String id = managerPanel.getTfEditId().getText();

				try {
					if (id.compareTo("") == 0) {
						infoBox("Introdu id!", "Avertizare!");
						return;
					} else {
						auxProduct.setId(Integer.parseInt(id));
					}
				} catch (Exception e) {
					infoBox("Id-ul trebuie sa fie numar!", "Avertizare!");
					return;
				}

				if (warehouse.getProduct(Integer.parseInt(id)) == null) {
					infoBox("Id-ul produsului NU exista!", "Avertizare!");
					return;
				}

				warehouse.removeProduct(auxProduct);
				cauta();
			}
		});
	}

	private void addProduct(WarehousePolicy policy) {
		Product product = new Product();
		boolean valid = true;
		String nume = managerPanel.getTfNume().getText();
		String categorie = managerPanel.getTfCategorie().getText();
		String descriere = managerPanel.getTfDescriere().getText();
		String pret = managerPanel.getTfPret().getText();
		String cantitate = managerPanel.getTfCantitate().getText();
		String errorMsg = "     Date de intrare invalide!\n\n";

		if (nume.compareTo("") == 0) {
			errorMsg += "->Introdu nume\n";
			valid = false;
		} else {
			product.setNume(nume);
		}

		if (categorie.compareTo("") == 0) {
			errorMsg += "->Introdu categorie\n";
			valid = false;
		} else {
			product.setCategorie(categorie);
		}

		if (descriere.compareTo("") == 0) {
			errorMsg += "->Introdu descriere\n";
			valid = false;
		} else {
			product.setDesciere(descriere);
		}

		try {
			if (pret.compareTo("") == 0) {
				errorMsg += "->Introdu pret\n";
				valid = false;
			} else {
				product.setPret(Double.parseDouble(pret));
			}
		} catch (Exception e) {
			errorMsg += "->Pretul-ul trebuie sa fie numar\n";
			valid = false;
		}
		try {
			if (cantitate.compareTo("") == 0) {
				errorMsg += "->Introdu cantitate\n";
				valid = false;
			} else {
				product.setCantitate(Integer.parseInt(cantitate));
			}
		} catch (Exception e) {
			errorMsg += "->Cantitate trebuie sa fie numar\n";
			valid = false;
		}

		if (valid) {
			warehouse.addProduct(product, policy);
			table.addObject(product);
		} else {
			infoBox(errorMsg, "Avertizare!");
		}
	}

	private void cauta() {
		managerPanel.getMyTable().addNewTable(
				new ArrayList<Object>(warehouse.cauta(managerPanel.getTfCauta()
						.getText())));
	}

	private void modificaProduct(WarehousePolicy policy) {
		String id = managerPanel.getTfEditId().getText();

		if (id.compareTo("") != 0) {
			Product product = new Product();
			String nume = managerPanel.getTfEditNume().getText();
			String categorie = managerPanel.getTfEditCategorie().getText();
			String descriere = managerPanel.getTfEditDescriere().getText();
			String pret = managerPanel.getTfEditPret().getText();
			String cantitate = managerPanel.getTfEditCantitate().getText();

			product.setId(Integer.parseInt(id));

			product.setNume(nume);
			product.setCategorie(categorie);
			product.setDesciere(descriere);

			try {
				product.setAbsolutPret(Double.parseDouble(pret));
			} catch (Exception e) {
				infoBox("Pretul-ul trebuie sa fie numar!", "Avertizare!");
				return;
			}
			try {
				product.setAbsolutCantitate(Integer.parseInt(cantitate));
			} catch (Exception e) {
				infoBox("Cantitate trebuie sa fie numar!!", "Avertizare!");
				return;
			}

			warehouse.addProduct(product, policy);
			table.revalidate();
			if (warehouse.existProduct(product)) {
				table.revalidate();
			} else {
				infoBox("Id-ul nu exista!", "Avertizare!");
			}
		} else {
			infoBox("Introdu id!", "Avertizare!");
		}
	}

	private static void infoBox(String infoMessage, String title) {
		JOptionPane.showMessageDialog(null, infoMessage, title,
				JOptionPane.INFORMATION_MESSAGE);
	}
}
