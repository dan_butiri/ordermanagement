package com.butirialexandrudan.ordermanagement;

import java.util.ArrayList;
import java.util.Comparator;

public class Tree<T> {
	/**
	 * 
	 * Enumeratie a ordinei de parcurgere a elementelor. ASC - crescator. DESC -
	 * descrescator.
	 * 
	 */
	public static enum TreeOrder {
		ASC, DESC
	};

	private TreeElement<T> root;
	private Comparator<T> comparator;
	private TreeElement<T> currentElement;
	private TreeOrder order;
	private int size;

	/**
	 * Creaza Creaza obiectul.
	 * 
	 * @param newComparator
	 *            un comparator pentru obiectele (<T>) care se vor stoca.
	 */
	public Tree(Comparator<T> newComparator) {
		comparator = newComparator;
		root = null;
		currentElement = null;
		order = TreeOrder.ASC;
		size = 0;
	}

	/**
	 * Returneaza un ArrayList cu toate obiectele stocate.
	 * 
	 * @return un ArrayList cu toate obiectele stocate
	 */
	public ArrayList<T> toArrayList() {
		ArrayList<T> aux = new ArrayList<T>();
		TreeElement<T> currentElement = order == TreeOrder.ASC ? this
				.getMinElement(root) : this.getMaxElement(root);

		while (currentElement != null) {
			aux.add(currentElement.getData());
			if (order == TreeOrder.ASC) {
				currentElement = this.succesor(currentElement);
			} else {
				currentElement = this.predecesor(currentElement);
			}
		}

		return aux;
	}

	/**
	 * Returneaza indexul elementului trimis ca si parametru.
	 * 
	 * @param element
	 *            elementul la care se doreste indexul
	 * @return indexul elementului trimis ca si parametru
	 */
	public int getIndexOf(T element) {
		TreeElement<T> currentElement = order == TreeOrder.ASC ? this
				.getMinElement(root) : this.getMaxElement(root);
		int index = 0;

		while (currentElement != null && currentElement.compareTo(element) != 0) {
			++index;
			if (order == TreeOrder.ASC) {
				currentElement = this.succesor(currentElement);
			} else {
				currentElement = this.predecesor(currentElement);
			}
		}

		return index;
	}

	/**
	 * Modifica campul "currentElement" sa indice urmatorul element stocat.
	 */
	public void nextElement() {
		if (order == TreeOrder.ASC) {
			currentElement = this.succesor(currentElement);
		} else {
			currentElement = this.predecesor(currentElement);
		}
	}

	/**
	 * Verifica daca campul "currentElement" este ultimul element stocat.
	 * 
	 * @return true daca nu este ultimul elemnt
	 */
	public boolean hasNext() {
		boolean aux = false;

		if (order == TreeOrder.ASC) {
			aux = (currentElement != null) ? true : false;
		} else {
			aux = (currentElement != null) ? true : false;
		}

		return aux;
	}

	/**
	 * Initializeaza campul "currentElement" sa indice spre cel mai mare element
	 * stocat sau cel mai mare in functie de campul "order".
	 */
	public void initNext() {
		currentElement = order == TreeOrder.ASC ? this.getMinElement(root)
				: this.getMaxElement(root);
	}

	/**
	 * Returneaza daca un element exista in tree (este stocat).
	 * 
	 * @param data
	 *            elementul care se doreste testat
	 * @return true daca exista
	 */
	public boolean exist(T data) {
		if (getTreeElement(data) == null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Returneaza un element "x" din lista in functie de un parametru.
	 * 
	 * @param k
	 * @return
	 */
	public T getElement(T k) {
		TreeElement<T> x = root;

		while (x != null && x.compareTo(k) != 0) {
			if (x.compareTo(k) < 0) {
				x = x.getChildRight();
			} else {
				x = x.getChildLeft();
			}
		}
		return x.getData();
	}

	/**
	 * Returneaza elementul maxim stocat.
	 * 
	 * @return elementul maxim
	 */
	public T getMax() {
		TreeElement<T> x = root;

		if (x != null) {
			while (x.getChildRight() != null) {
				x = x.getChildRight();
			}

			return x.getData();
		}

		return null;
	}

	/**
	 * Returneaza elementul minim stocat.
	 * 
	 * @return elementul minim
	 */
	public T getMin() {
		TreeElement<T> x = root;

		if (x != null) {
			while (x.getChildLeft() != null) {
				x = x.getChildLeft();
			}

			return x.getData();
		}

		return null;
	}

	/**
	 * Adauga un nou element.
	 * 
	 * @param data
	 *            elementul care se adauga
	 */
	public void addElement(T data) {
		TreeElement<T> element = new TreeElement<T>(data, comparator);
		TreeElement<T> p = null;
		TreeElement<T> x = root;

		while (x != null) {
			p = x;
			if (element.compareTo(x.getData()) < 0) {
				x = x.getChildLeft();
			} else {
				x = x.getChildRight();
			}
		}
		element.setOldMan(p);
		if (p == null) {
			root = element;
		} else if (element.compareTo(p.getData()) < 0) {
			p.setChildLeft(element);
		} else {
			p.setChildRight(element);
		}
		++size;

	}

	/**
	 * Sterge un element.
	 * 
	 * @param data
	 *            elementul care se doreste sters
	 */
	public void deleteElement(T data) {
		TreeElement<T> element = getTreeElement(data);
		TreeElement<T> y = null;
		TreeElement<T> x = null;

		if (element != null) {
			if (element.getChildLeft() == null
					|| element.getChildRight() == null) {
				y = element;
			} else {
				y = succesor(element);
			}
			if (y.getChildLeft() != null) {
				x = y.getChildLeft();
			} else {
				x = y.getChildRight();
			}
			if (x != null) {
				x.setOldMan(y.getOldMan());
			}
			if (y.getOldMan() == null) {
				root = x;
			} else if (y == y.getOldMan().getChildLeft()) {
				y.getOldMan().setChildLeft(x);
			} else {
				y.getOldMan().setChildRight(x);
			}
			if (y != element) {
				element.setData(y.getData());
			}
			--size;
		}
	}

	/**
	 * Returneaza ordinea curenta de parcurgere (crescator/descrescator).
	 * 
	 * @return ordinea curenta de parcurgere
	 */
	public TreeOrder getOrder() {
		return order;
	}

	/**
	 * Seteaza ordinea de parcurgere (crescator/descrescator).
	 * 
	 * @param order
	 *            ordinea de parcurgere (crescator/descrescator)
	 */
	public void setOrder(TreeOrder order) {
		this.order = order;
	}

	/**
	 * Returneaza elementul curent. Aceasta metoda se foloseste impreuna cu
	 * metodele : initNext() , hasNext(), nextElement().
	 * 
	 * @return elementul curent
	 */
	public T getCurrentElement() {
		return currentElement.getData();
	}

	/**
	 * Returneaza numarul de elemente stocate.
	 * 
	 * @return numarul de elemente stocate
	 */
	public int getSize() {
		return size;
	}

	private TreeElement<T> succesor(TreeElement<T> x) {
		TreeElement<T> y;

		if (x != null) {
			if (x.getChildRight() != null) {
				return this.getMinElement(x.getChildRight());
			}
			y = x.getOldMan();
			while (y != null && x == y.getChildRight()) {
				x = y;
				y = y.getOldMan();
			}

			return y;
		}

		return x;
	}

	private TreeElement<T> predecesor(TreeElement<T> x) {
		TreeElement<T> y;

		if (x != null) {
			if (x.getChildLeft() != null) {
				return this.getMaxElement(x.getChildLeft());
			}
			y = x.getOldMan();
			while (y != null && x == y.getChildLeft()) {
				x = y;
				y = y.getOldMan();
			}

			return y;
		}

		return x;
	}

	private TreeElement<T> getMinElement(TreeElement<T> root) {
		TreeElement<T> x = root;

		if (x != null) {
			while (x.getChildLeft() != null) {
				x = x.getChildLeft();
			}
		}

		return x;
	}

	private TreeElement<T> getMaxElement(TreeElement<T> root) {
		TreeElement<T> x = root;

		if (x != null) {
			while (x.getChildRight() != null) {
				x = x.getChildRight();
			}
		}

		return x;
	}

	private TreeElement<T> getTreeElement(T k) {
		TreeElement<T> x = root;

		while (x != null && x.compareTo(k) != 0) {
			if (x.compareTo(k) < 0) {
				x = x.getChildRight();
			} else {
				x = x.getChildLeft();
			}
		}
		return x;
	}
}
