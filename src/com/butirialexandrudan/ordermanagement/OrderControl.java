package com.butirialexandrudan.ordermanagement;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class OrderControl {

	private OrderPanel orderPanel;
	private JTable table;
	private MyTable myTable;
	private DefaultListModel<String> dlm;
	private Warehouse warehouse;
	private CustomerList customerList;
	private double pretTotal;
	private OPDep oPDep;

	/**
	 * Creaza obiectul.
	 * 
	 * @param newMainFrame
	 *            fereastrea principala
	 * @param newWarehouse
	 *            lista cu produse
	 * @param newCustomerList
	 *            lista cu clientii
	 * @param newOPDep
	 *            lista cu comenzi
	 */
	public OrderControl(MainFrame newMainFrame, Warehouse newWarehouse,
			CustomerList newCustomerList, OPDep newOPDep) {
		this.orderPanel = newMainFrame.getMainPanel().getOrderPanel();
		this.table = newMainFrame.getMainPanel().getOrderPanel().getTable();
		this.myTable = newMainFrame.getMainPanel().getOrderPanel().getMyTable();
		this.dlm = newMainFrame.getMainPanel().getOrderPanel().getDlm();
		this.warehouse = newWarehouse;
		this.customerList = newCustomerList;
		this.pretTotal = 0;
		this.oPDep = newOPDep;

		orderPanel.getBtnAddProduct().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				addProduct();
			}
		});

		orderPanel.getBtnRemoveProduct().addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent arg) {
						removeProduct();
					}
				});

		orderPanel.getBtnExecutaComanda().addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						executaComanda();
					}
				});

		orderPanel.getBtnCauta().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				myTable.addNewTable(new ArrayList<Object>(warehouse
						.cauta(orderPanel.getTfCauta().getText())));
			}
		});

		orderPanel.getTfCauta().addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent arg) {

				if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
					myTable.addNewTable(new ArrayList<Object>(warehouse
							.cauta(orderPanel.getTfCauta().getText())));
				}

			}

			public void keyReleased(KeyEvent arg) {
			}

			public void keyTyped(KeyEvent arg) {
			}
		});

	}

	private void addProduct() {
		int aux[] = table.getSelectedRows();
		int id = 0;
		Product product = null;
		int cantitateCeruta = 0;

		try {
			cantitateCeruta = Integer.parseInt(orderPanel.getTfCantitate()
					.getText());
		} catch (Exception e) {
			infoBox("Cantitatea trebuie sa fie numar...", "Avertizare!");
			return;
		}

		for (int i = aux.length - 1; i >= 0; --i) {
			try {
				id = Integer.parseInt(myTable.getValueAt(aux[i], 0).toString());
			} catch (Exception e) {
				System.out.println("OrderControl -> addProduct()");
			}
			product = warehouse.getProduct(id);
			if (cantitateCeruta > 0
					&& cantitateCeruta <= product.getCantitate()) {
				pretTotal += (product.getPret() * cantitateCeruta);
				product.setCantitate(product.getCantitate() - cantitateCeruta);
				dlm.addElement(orderString(product.getId(), product.getNume(),
						cantitateCeruta, product.getPret()));
				orderPanel.getLblTotal().setText(
						String.format("Total:%10.2f", pretTotal));
				orderPanel.getLblNrArticole().setText(
						String.format("Nr. produse:%10d", dlm.getSize()));
				myTable.revalidate();
			} else {
				infoBox("Cantitatea ceruta pentru produsul \""
						+ product.getNume() + "\" nu se poate onora...",
						"Avertizare!");
			}
		}
	}

	private void removeProduct() {
		int aux[] = orderPanel.getOrderList().getSelectedIndices();
		int id = 0;
		int cantitate = 0;
		Product product = null;

		for (int i = aux.length - 1; i >= 0; --i) {
			String stringAux[] = dlm.get(aux[i]).split(" +");
			try {
				id = Integer.parseInt(stringAux[0]);
				cantitate = Integer.parseInt(stringAux[2]);
			} catch (Exception e) {

			}
			product = warehouse.getProduct(id);
			product.setCantitate(product.getCantitate() + cantitate);
			pretTotal -= (product.getPret() * cantitate);
			myTable.revalidate();
			dlm.remove(aux[i]);
			orderPanel.getLblTotal().setText(
					String.format("Total:%10.2f", pretTotal));
			orderPanel.getLblNrArticole().setText(
					String.format("Nr. produse:%10d", dlm.getSize()));
		}
	}

	private void executaComanda() {
		int customerId = 0;
		Order order = new Order();
		Product product;

		try {
			customerId = Integer.parseInt(orderPanel.getTfCustomerId()
					.getText());
		} catch (Exception e) {
			infoBox("Id-ul trebuie sa fie numar...", "Avertizare!");
			return;
		}

		if (dlm.size() == 0) {
			infoBox("Lista de cumparaturi nu poate sa fie goala...",
					"Avertizare!");
			return;
		}

		if (customerList.getCustoer(customerId) == null) {
			infoBox("Id-ul \"" + orderPanel.getTfCustomerId().getText()
					+ "\" nu exista...", "Avertizare!");
			return;
		}

		try {
			for (int i = 0; i < dlm.getSize(); ++i) {
				Product newProduct = new Product();
				String stringAux[] = dlm.get(i).split(" +");
				product = warehouse.getProduct(Integer.parseInt(stringAux[0]));
				newProduct.setId(product.getId());
				newProduct.setNume(product.getNume());
				newProduct.setPret(product.getPret());
				newProduct.setCantitate(Integer.parseInt(stringAux[2]));
				newProduct.setCategorie(product.getCategorie());
				newProduct.setDesciere(product.getDesciere());
				order.addProduct(newProduct);
				product.setRank(product.getRank() + 1);
			}
		} catch (Exception e) {

		}
		order.setIdCustoer(customerId);
		order.setPretTotal(pretTotal);
		order.setDate(new Date());
		oPDep.addOrder(order);
		order.setKey(oPDep.getLastKey());
		dlm.removeAllElements();
		pretTotal = 0;
		orderPanel.getLblTotal()
				.setText(String.format("Total:%10.2f", pretTotal));
		orderPanel.getLblNrArticole().setText(
				String.format("Nr. produse:%10d", dlm.getSize()));
	}

	private static String orderString(int id, String nume, int cantitate,
			double pret) {
		return String.format("%-20d%-50s%-20d%20.2f", id, nume, cantitate, pret);
	}

	private static void infoBox(String infoMessage, String title) {
		JOptionPane.showMessageDialog(null, infoMessage, title,
				JOptionPane.INFORMATION_MESSAGE);
	}

}
