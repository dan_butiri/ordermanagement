package com.butirialexandrudan.ordermanagement;

public class Main {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		Warehouse warehouse = new Warehouse();
		CustomerList customerList = new CustomerList();
		OPDep oPDep = new OPDep(customerList, warehouse);
		MainFrame frame = new MainFrame(warehouse, customerList, oPDep);

		new WarehouseControl(frame, warehouse);
		new CustomerControl(frame, customerList);
		new OrderControl(frame, warehouse, customerList, oPDep);
		new OPDepControl(frame, customerList, oPDep);

		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocation(200, 50);
		frame.setTitle("Order Management");
		frame.pack();

	}
}
