package com.butirialexandrudan.ordermanagement;

import java.util.ArrayList;
import java.util.Date;

public class Order {
	ArrayList<Product> product;
	@AddThis
	int key;
	@AddThis
	int idCustoer;
	@AddThis
	Date date;
	@AddThis
	double pretTotal;

	/**
	 * Creaza obiectul.
	 */
	public Order() {
		product = new ArrayList<Product>();
		idCustoer = 0;
		date = new Date();
		pretTotal = 0;
	}

	/**
	 * Returneaza un ArrayList cu produsele existente in comanda.
	 * 
	 * @return un ArrayList cu produsele existente in comanda
	 */
	public ArrayList<Product> getProduct() {
		return product;
	}

	/**
	 * Adauga un nou produs la comanda.
	 * 
	 * @param product
	 *            produsul care se adauga
	 */
	public void addProduct(Product product) {
		this.product.add(product);
	}

	/**
	 * Returneaza id-ul clientului care a trimis comanda.
	 * 
	 * @return id-ul clientului care a trimis comanda
	 */
	public int getIdCustoer() {
		return idCustoer;
	}

	/**
	 * Seteaza id-ul clientului.
	 * 
	 * @param idCustoer
	 *            id-ul clientului
	 */
	public void setIdCustoer(int idCustoer) {
		this.idCustoer = idCustoer;
	}

	/**
	 * Returneaza data la care a fost inregistrata comanda.
	 * 
	 * @return data la care a fost inregistrata comanda
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Seteaza data la inregistrari.
	 * 
	 * @param date
	 *            data la inregistrari
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Returneaza pretul total comenzi.
	 * 
	 * @return pretul total comenzi
	 */
	public double getPretTotal() {
		return pretTotal;
	}

	/**
	 * Seteaza pretul total al comenzi.
	 * 
	 * @param pretTotal
	 *            pretul total
	 */
	public void setPretTotal(double pretTotal) {
		this.pretTotal = pretTotal;
	}

	/**
	 * Returneaza cheia comenzi.
	 * 
	 * @return cheia comenzi
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Seteaza cheia comenzi.
	 * 
	 * @param key
	 *            cheia comenzi
	 */
	public void setKey(int key) {
		this.key = key;
	}
}
