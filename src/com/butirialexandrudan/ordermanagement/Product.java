package com.butirialexandrudan.ordermanagement;

public class Product {
	@AddThis
	private int id;
	@AddThis
	private String nume;
	@AddThis
	private double pret;
	@AddThis
	private int rank;
	@AddThis
	private int cantitate;
	@AddThis
	private String categorie;
	@AddThis
	private String desciere;

	/**
	 * Creaza un produs.
	 */
	public Product() {
		id = 0;
		nume = "Articol" + id;
		pret = 0;
		rank = 0;
		categorie = "Diverse";
		desciere = "Fara descriere...";
	}

	/**
	 * Creaza un produs si seteaza numele.
	 * 
	 * @param nume
	 *            numele produsului
	 */
	public Product(String nume) {
		id = 0;
		this.nume = nume;
		pret = 0;
		rank = 0;
		categorie = "Diverse";
		desciere = "Fara descriere...";
	}

	/**
	 * Returneaza id-ul produsului.
	 * 
	 * @return id-ul produsului
	 */
	public int getId() {
		return id;
	}

	/**
	 * Seteaza id-ul produsului.
	 * 
	 * @param id
	 *            id-ul produsului
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Returneaza numele produsului.
	 * 
	 * @return numele produsului
	 */
	public String getNume() {
		return nume;
	}

	/**
	 * Seteaza numelel produsului.
	 * 
	 * @param nume
	 *            numele produsului
	 */
	public void setNume(String nume) {
		this.nume = nume;
	}

	/**
	 * Returneaza pretul produsului.
	 * 
	 * @return pretul produsului
	 */
	public double getPret() {
		return pret;
	}

	/**
	 * Seteaza pretul produsului. Daca este o valoare negativa pretul se pune pe
	 * 0.
	 * 
	 * @param pret
	 *            pretul produsului
	 */
	public void setPret(double pret) {
		this.pret = pret > 0 ? pret : 0;
	}

	/**
	 * Seteaza pretul produsului.
	 * 
	 * @param pret
	 *            pretul produsului
	 */
	public void setAbsolutPret(double pret) {
		this.pret = pret;
	}

	/**
	 * Returneaza rank-ul produsului.
	 * 
	 * @return rank-ul produsului
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * Seteaza rank-ul produsului. Daca este o valoare negativa rank-ul se pune
	 * pe 0.
	 * 
	 * @param rank
	 *            rank-ul produsului
	 */
	public void setRank(int rank) {
		this.rank = rank > 0 ? rank : 0;
	}

	/**
	 * Seteaza rank-ul produsului.
	 * 
	 * @param rank
	 *            rank-ul produsului
	 */
	public void setAbsolutRank(int rank) {
		this.rank = rank;
	}

	/**
	 * Returneaza cantitata produsului.
	 * 
	 * @return cantitata produsului
	 */
	public int getCantitate() {
		return cantitate;
	}

	/**
	 * Seteaza cantitatea produsului. Daca este o valoare negativa cantitatea se
	 * pune pe 0.
	 * 
	 * @param cantitate
	 *            cantitatea produsului
	 */
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate > 0 ? cantitate : 0;
	}

	/**
	 * Seteaza cantitatea produsului.
	 * 
	 * @param cantitate
	 *            cantitatea produsului
	 */
	public void setAbsolutCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	/**
	 * Returneaza categoria produsului.
	 * 
	 * @return categoria produsului
	 */
	public String getCategorie() {
		return categorie;
	}

	/**
	 * Seteaza categoria produsului.
	 * 
	 * @param categorie
	 *            categoria produsului
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	/**
	 * Returneaza desciera produsului.
	 * 
	 * @return desciera produsului
	 */
	public String getDesciere() {
		return desciere;
	}

	/**
	 * Seteaza descrierea produsului.
	 * 
	 * @param desciere
	 *            descrierea produsului
	 */
	public void setDesciere(String desciere) {
		this.desciere = desciere;
	}

}
