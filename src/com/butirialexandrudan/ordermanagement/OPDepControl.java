package com.butirialexandrudan.ordermanagement;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.DefaultListModel;
import javax.swing.JTable;

public class OPDepControl {

	private OPDepPanel oPDepPanel;
	private CustomerList customerList;
	private OPDep oPDep;
	private JTable table;
	private MyTable myTable;
	private DefaultListModel<String> dlm;

	/**
	 * Creaza obiectul.
	 * 
	 * @param newMainFrame
	 *            fereastrea principala
	 * @param newCustomerList
	 *            lista cu clientii
	 * @param newOPDep
	 *            lista cu comenzi
	 */
	public OPDepControl(MainFrame newMainFrame, CustomerList newCustomerList,
			OPDep newOPDep) {
		this.oPDepPanel = newMainFrame.getMainPanel().getoPDepPanel();
		this.customerList = newCustomerList;
		this.oPDep = newOPDep;

		table = oPDepPanel.getTable();
		myTable = oPDepPanel.getMyTable();
		dlm = oPDepPanel.getDlm();

		table.addMouseMotionListener(new MouseMotionListener() {

			public void mouseMoved(MouseEvent arg) {
			}

			public void mouseDragged(MouseEvent arg) {

				int key = Integer.parseInt(myTable.getValueAt(
						table.getSelectedRow(), 0).toString());
				completeazaDetaliiList(oPDep.getElement(key));

			}
		});

		table.addMouseListener(new MouseListener() {

			public void mouseReleased(MouseEvent arg) {
				int key = Integer.parseInt(myTable.getValueAt(
						table.getSelectedRow(), 0).toString());
				completeazaDetaliiList(oPDep.getElement(key));
			}

			public void mousePressed(MouseEvent arg) {
				int key = Integer.parseInt(myTable.getValueAt(
						table.getSelectedRow(), 0).toString());
				completeazaDetaliiList(oPDep.getElement(key));
			}

			public void mouseExited(MouseEvent arg) {
			}

			public void mouseEntered(MouseEvent arg) {
			}

			public void mouseClicked(MouseEvent arg) {
			}
		});

		table.addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent ke) {
			}

			public void keyReleased(KeyEvent ke) {
				if (ke.getKeyCode() == KeyEvent.VK_UP
						|| ke.getKeyCode() == KeyEvent.VK_DOWN) {
					int key = Integer.parseInt(myTable.getValueAt(
							table.getSelectedRow(), 0).toString());
					completeazaDetaliiList(oPDep.getElement(key));
				}
			}

			public void keyPressed(KeyEvent ke) {
				if (ke.getKeyCode() == KeyEvent.VK_UP
						|| ke.getKeyCode() == KeyEvent.VK_DOWN) {
					int key = Integer.parseInt(myTable.getValueAt(
							table.getSelectedRow(), 0).toString());
					completeazaDetaliiList(oPDep.getElement(key));
				}
			}
		});

		oPDepPanel.getBtnOnorat().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg) {
				if (table.getSelectedRow() >= 0) {
					int key = Integer.parseInt(myTable.getValueAt(
							table.getSelectedRow(), 0).toString());
					myTable.removeObject(table.getSelectedRow());
					oPDep.removeElement(key);
					dlm.removeAllElements();
				}
			}
		});
	}

	private void completeazaDetaliiList(Order order) {
		Customer customer = customerList.getCustoer(order.getIdCustoer());

		dlm.removeAllElements();
		dlm.addElement("------------------Despre client----------------------");
		dlm.addElement("Nume: " + customer.getNume());
		dlm.addElement("Adresa: " + customer.getAdresa());
		dlm.addElement("Varsta: " + customer.getVarsta());
		dlm.addElement("------------------Data comenzi-----------------------");
		dlm.addElement(order.getDate().toString());
		dlm.addElement("------------------Produse comandate------------");
		for (Product product : order.getProduct()) {
			dlm.addElement("Nume: " + product.getNume());
			dlm.addElement("Categoria: " + product.getCategorie());
			dlm.addElement("Cantitate: " + product.getCantitate());
			dlm.addElement("Pret produs: " + product.getPret());
			dlm.addElement("Pret produs * cantitate: "
					+ (product.getPret() * product.getCantitate()));
			dlm.addElement("-----------------------------------------------------------");
		}
		dlm.addElement("Pret total: " + order.getPretTotal());
	}
}
