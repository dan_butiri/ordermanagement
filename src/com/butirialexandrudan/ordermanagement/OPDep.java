package com.butirialexandrudan.ordermanagement;

import java.util.ArrayList;
import java.util.TreeMap;

public class OPDep {
	TreeMap<Integer, Order> orderList;
	CustomerList customerList;
	Warehouse warehosue;

	/**
	 * Creaza obiectul.
	 * 
	 * @param newCustomerList
	 *            lista cu clieti.
	 * @param newWarehosue
	 *            lista produselor.
	 */
	public OPDep(CustomerList newCustomerList, Warehouse newWarehosue) {
		this.orderList = new TreeMap<Integer, Order>();
		this.customerList = newCustomerList;
		this.warehosue = newWarehosue;
	}

	/**
	 * Returneaza elementul care are asociata valoarea specificata ca parametru.
	 * 
	 * @param key
	 *            cheia elementului cautat.
	 * @return elementul care are cheia "key"
	 */
	public Order getElement(int key) {
		return orderList.get(key);
	}

	/**
	 * Sterge elementuul care are asociata o anumita valoare "key".
	 * 
	 * @param key
	 *            cheia elementului care se doreste sters
	 */
	public void removeElement(int key) {
		this.orderList.remove(key);
	}

	/**
	 * Returneaza ultima cheie inregistrata.
	 * 
	 * @return ultima cheie inregistrata
	 */
	public int getLastKey() {
		return orderList.lastKey().intValue();
	}

	/**
	 * Adauga o noua comanda.
	 * 
	 * @param newOrder
	 *            comanda care se adauga
	 */
	public void addOrder(Order newOrder) {
		if (orderList.isEmpty()) {
			orderList.put(0, newOrder);
		} else {
			orderList.put(orderList.lastKey() + 1, newOrder);
		}
	}

	/**
	 * Returneaza un ArrayList cu toate elementele stocate.
	 * 
	 * @return un ArrayList cu toate elementele stocate
	 */
	public ArrayList<Order> toArrayList() {
		ArrayList<Order> aux = new ArrayList<Order>();

		for (Integer key : orderList.keySet()) {
			aux.add(orderList.get(key));
		}

		return aux;
	}
}
