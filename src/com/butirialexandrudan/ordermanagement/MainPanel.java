package com.butirialexandrudan.ordermanagement;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {

	private JTabbedPane tabbedPane;
	private WarehousePanel warehousePanel;
	private CustomerPanel customerPanel;
	private OrderPanel orderPanel;
	private OPDepPanel oPDepPanel;
	private Warehouse warehouse;
	private CustomerList customerList;
	private OPDep oPDep;

	/**
	 * Create the panel.
	 */
	public MainPanel(Warehouse newWarehouse, CustomerList newCustomerList,
			OPDep newOPDep) {
		this.warehouse = newWarehouse;
		this.customerList = newCustomerList;
		this.oPDep = newOPDep;

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		warehousePanel = new WarehousePanel(warehouse);
		customerPanel = new CustomerPanel(customerList);
		orderPanel = new OrderPanel(warehouse);
		oPDepPanel = new OPDepPanel(oPDep);

		this.setLayout(new GridLayout(1, 1));

		tabbedPane.addTab("Customer", customerPanel);
		tabbedPane.addTab("Product", warehousePanel);
		tabbedPane.addTab("Order", orderPanel);
		tabbedPane.addTab("OPDep", oPDepPanel);
		add(tabbedPane);

		tabbedPane.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent ce) {

				if (tabbedPane.getSelectedIndex() == 0) { // Customer
					customerPanel.getMyTable().addNewTable(
							new ArrayList<Object>(customerList.toArrayList()));
				}

				if (tabbedPane.getSelectedIndex() == 1) { // Product
					warehousePanel.getMyTable().addNewTable(
							new ArrayList<Object>(warehouse.toArrayList()));
				}
				if (tabbedPane.getSelectedIndex() == 2) { // Order
					orderPanel.getMyTable().addNewTable(
							new ArrayList<Object>(warehouse.toArrayList()));
				}
				if (tabbedPane.getSelectedIndex() == 3) { // OPDep
					oPDepPanel.getMyTable().addNewTable(
							new ArrayList<Object>(oPDep.toArrayList()));
				}

			}
		});
	}

	/**
	 * Returneaza panel-ul care se ocupa de procesarea produselor
	 * (warehousePanel).
	 * 
	 * @return panel-ul care se ocupa de procesarea produselor
	 */
	public WarehousePanel getManagerPanel() {
		return warehousePanel;
	}

	/**
	 * Returneaza panel-ul care se ocupa de procesarea clientilor
	 * (customerPanel).
	 * 
	 * @return panel-ul care se ocupa de procesarea clientilor
	 */
	public CustomerPanel getCustomerPanel() {
		return customerPanel;
	}

	/**
	 * Returneaza panel-ul care se ocupa de preluarea comenzilor (orderPanel).
	 * 
	 * @return panel-ul care se ocupa de preluarea comenzilor
	 */
	public OrderPanel getOrderPanel() {
		return orderPanel;
	}

	/**
	 * Returneaza panel-ul care se ocupa de procesarea comenzilor (oPDepPanel).
	 * 
	 * @return panel-ul care se ocupa de procesarea comenzilor
	 */
	public OPDepPanel getoPDepPanel() {
		return oPDepPanel;
	}
}
