package com.butirialexandrudan.ordermanagement;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public class OrderPanel extends JPanel {

	private GridBagConstraints gbc;
	private MyTable myTable;
	private JTable table;
	private JScrollPane scrollPaneTable;
	private DefaultListModel<String> dlm;
	private JList<String> orderList;
	private JScrollPane scrollPaneOrder;
	private JButton btnCauta = new JButton("Cauta");
	private JButton btnAddProduct = new JButton("Adauga");
	private JButton btnRemoveProduct = new JButton("Sterge");
	private JButton btnExecutaComanda = new JButton("Comanda!");
	private JTextField tfCauta = new JTextField();
	private JTextField tfCustomerId = new JTextField();
	private JTextField tfCantitate = new JTextField("1");
	private JLabel lblTotal = new JLabel("Total:         0");
	private JLabel lblNrArticole = new JLabel("Nr. produse:         0");
	private JLabel lblCustomerId = new JLabel("Id client:");
	private JLabel lblCantitate = new JLabel("Cantitate:");

	/**
	 * Create the panel.
	 * 
	 * @param warehouse
	 *            lista cu produse
	 */
	public OrderPanel(Warehouse warehouse) {
		gbc = new GridBagConstraints();
		myTable = new MyTable(Product.class, AddThis.class,
				new ArrayList<Object>(warehouse.toArrayList()));
		table = new JTable(myTable);
		scrollPaneTable = new JScrollPane(table);
		dlm = new DefaultListModel<String>();
		orderList = new JList<String>(dlm);
		scrollPaneOrder = new JScrollPane(orderList);

		orderList
				.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		this.setLayout(new GridBagLayout());
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;

		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(lblCustomerId, gbc);

		gbc.gridwidth = 2;
		gbc.gridheight = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		this.add(tfCustomerId, gbc);

		gbc.weightx = 0.2;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 3;
		gbc.gridy = 0;
		this.add(lblCantitate, gbc);

		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 4;
		gbc.gridy = 0;
		this.add(tfCantitate, gbc);

		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 5;
		gbc.gridy = 0;
		this.add(btnAddProduct, gbc);

		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 6;
		gbc.gridy = 0;
		this.add(btnRemoveProduct, gbc);

		gbc.weighty = 0.2;
		gbc.gridwidth = 3;
		gbc.gridheight = 30;
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(scrollPaneTable, gbc);

		gbc.gridwidth = 4;
		gbc.gridheight = 30;
		gbc.gridx = 3;
		gbc.gridy = 1;
		this.add(scrollPaneOrder, gbc);

		gbc.weighty = 0.0;
		gbc.gridwidth = 2;
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 32;
		this.add(tfCauta, gbc);

		gbc.weightx = 0.0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 2;
		gbc.gridy = 32;
		this.add(btnCauta, gbc);

		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 3;
		gbc.gridy = 32;
		this.add(lblTotal, gbc);

		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridx = 4;
		gbc.gridy = 32;
		this.add(lblNrArticole, gbc);

		gbc.gridwidth = 2;
		gbc.gridheight = 1;
		gbc.gridx = 5;
		gbc.gridy = 32;
		this.add(btnExecutaComanda, gbc);
	}

	/**
	 * Returneaza obiectul care retine elementele care sunt puse in tabel
	 * (myTable).
	 * 
	 * @return obiectul care retine elementele care sunt puse in tabel
	 */
	public MyTable getMyTable() {
		return myTable;
	}

	/**
	 * Returneaza tabelul in care sunt afisate produsele (table).
	 * 
	 * @return tabelul in care sunt afisate produsele
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * Returneaza lista in care este stocata lista cu detaliile despre produsele
	 * comandate.
	 * 
	 * @return lista in care este stocata lista cu detaliile despre produsele
	 *         comandate
	 */
	public DefaultListModel<String> getDlm() {
		return dlm;
	}

	/**
	 * Returneaza lista in care sunt afisate detaliile despre produsele
	 * comandate
	 * 
	 * @return lista in care sunt afisate detaliile despre produsele comandate
	 */
	public JList<String> getOrderList() {
		return orderList;
	}

	/**
	 * Returneaza butonul de cautare produs.
	 * 
	 * @return butonul de cautare produs
	 */
	public JButton getBtnCauta() {
		return btnCauta;
	}

	/**
	 * Returneaza butonul de adaugare produse in lista de comanda.
	 * 
	 * @return butonul de adaugare produse
	 */
	public JButton getBtnAddProduct() {
		return btnAddProduct;
	}

	/**
	 * Returneaza butonul de stergere produse din lista de comanda.
	 * 
	 * @return butonul de stergere produse
	 */
	public JButton getBtnRemoveProduct() {
		return btnRemoveProduct;
	}

	/**
	 * Returneaza butonul de executare comanda.
	 * 
	 * @return butonul de executare comanda
	 */
	public JButton getBtnExecutaComanda() {
		return btnExecutaComanda;
	}

	/**
	 * Returneaza TextField-ul in care se scrie numele produsului cautat
	 * (tfCauta).
	 * 
	 * @return TextField-ul in care se scrie numele produsului cautat
	 */
	public JTextField getTfCauta() {
		return tfCauta;
	}

	/**
	 * Returneaza TextField-ul cu id-ul clientului care trimite comanda.
	 * 
	 * @return TextField cu id-ul clientului
	 */
	public JTextField getTfCustomerId() {
		return tfCustomerId;
	}

	/**
	 * Returneaza Label-ul in care este afisat pretul total al comenzi.
	 * 
	 * @return Label in care este afisat pretul total
	 */
	public JLabel getLblTotal() {
		return lblTotal;
	}

	/**
	 * Returneaza Label-ul in care este afisat numarul de produse comandate.
	 * 
	 * @return Label in care este afisat numarul de produse comandate
	 */
	public JLabel getLblNrArticole() {
		return lblNrArticole;
	}

	/**
	 * Returneaza TextField-ul in care este specificata cantitatea dorita.
	 * 
	 * @return TextField in care este specificata cantitatea dorita
	 */
	public JTextField getTfCantitate() {
		return tfCantitate;
	}
}
