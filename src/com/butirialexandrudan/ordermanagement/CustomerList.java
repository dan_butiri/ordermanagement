package com.butirialexandrudan.ordermanagement;

import java.util.ArrayList;
import java.util.TreeSet;

public class CustomerList {
	TreeSet<Customer> customerList;

	/**
	 * Creaza obiectul.
	 */
	public CustomerList() {
		customerList = new TreeSet<Customer>();
	}

	/**
	 * Adauga un nou client.
	 * 
	 * @param newCustomer
	 *            clientul care se doreste adaugat.
	 */
	public void addCustomer(Customer newCustomer) {
		if (customerList.isEmpty()) {
			newCustomer.setId(0);
		} else {
			newCustomer.setId(customerList.last().getId() + 1);
		}
		customerList.add(newCustomer);
	}

	/**
	 * Sterge un client. Specificarea clientului se face cu ajutorul unui id.
	 * 
	 * @param removeId
	 *            id-ul clientului care se sterge.
	 */
	public void removeCustomer(int removeId) {
		for (Customer customer : customerList) {
			if (customer.getId() == removeId) {
				customerList.remove(customer);
				return;
			}
		}
	}

	/**
	 * Returneaza ultimul client adaugat.
	 * 
	 * @return ultimul client adaugat
	 */
	public Customer getLastCustomer() {
		return customerList.last();
	}

	/**
	 * Returneaza o lista a clientilor adaugati.
	 * 
	 * @return o lista a clientilor adaugati
	 */
	public ArrayList<Customer> toArrayList() {
		ArrayList<Customer> aux = new ArrayList<Customer>();

		for (Customer customer : customerList) {
			aux.add(customer);
		}

		return aux;
	}

	/**
	 * Returneaza o lista cu clienti care contin in nume caracterele specificate
	 * prin parametrul ch.
	 * 
	 * @param ch
	 *            un sir de caractere dupa care se face cautarea.
	 * @return o lista cu clienti care contin in nume caracterele specificate
	 *         prin parametru
	 */
	public ArrayList<Customer> cauta(String ch) {
		ArrayList<Customer> aux = new ArrayList<Customer>();

		for (Customer customer : customerList) {
			if (customer.getNume().toLowerCase().contains(ch.toLowerCase())) {
				aux.add(customer);
			}
		}

		return aux;
	}

	/**
	 * Returneaza un client in functie de id. Id-ul este specificat ca
	 * parametru.
	 * 
	 * @param id
	 *            id-ul clentului cautat.
	 * @return clientul care are id-ul specificate ca parametru.
	 */
	public Customer getCustoer(int id) {

		for (Customer customer : customerList) {
			if (customer.getId() == id) {
				return customer;
			}
		}

		return null;
	}
}
