package com.butirialexandrudan.ordermanagement;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private MainPanel mainPanel;

	/**
	 * Create the frame.
	 */
	public MainFrame(Warehouse warehouse, CustomerList customerList, OPDep oPDep) {

		mainPanel = new MainPanel(warehouse, customerList, oPDep);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setContentPane(mainPanel);
	}

	/**
	 * Returneaza panel-ul principal (mainPanel).
	 * 
	 * @return panel-ul principal
	 */
	public MainPanel getMainPanel() {
		return mainPanel;
	}
}
