package com.butirialexandrudan.ordermanagement;

import java.util.ArrayList;
import java.util.Comparator;

public class Warehouse {
	private static Comparator<Product> comp = new Comparator<Product>() {

		public int compare(Product product1, Product product2) {
			// return
			// product1.getNume().compareToIgnoreCase(product2.getNume());
			return Integer.compare(product1.getId(), product2.getId());
		}
	};

	/**
	 * Enumeratie pentru modul de introducere al elementelor in Warehouse.
	 * NEW_PRODUCT - inserare pe o noua pozitie. APPEND_PRODUCT - modificarea
	 * unui element existent, adaugare de nou element daca nu exista.
	 */
	public static enum WarehousePolicy {
		NEW_PRODUCT, APPEND_PRODUCT
	};

	private Tree<Product> productTree;
	private int nexId = 0;

	/**
	 * Creaza obiectul.
	 */
	public Warehouse() {
		productTree = new Tree<Product>(comp);
	}

	/**
	 * Adauga un nou produs.
	 * 
	 * @param newProduct
	 *            produsul adaugat
	 * @param policy
	 *            modul de adaugare
	 */
	public void addProduct(Product newProduct, WarehousePolicy policy) {
		if (policy == WarehousePolicy.APPEND_PRODUCT
				&& productTree.exist(newProduct)) {
			Product aux = productTree.getElement(newProduct);
			String nume = newProduct.getNume();
			String categorie = newProduct.getCategorie();
			String descriere = newProduct.getDesciere();
			double pret = newProduct.getPret();
			int cantitate = newProduct.getCantitate();

			if (nume.compareTo("") != 0) {
				aux.setNume(newProduct.getNume());
			}
			if (categorie.compareTo("") != 0) {
				aux.setCategorie(newProduct.getCategorie());
			}
			if (descriere.compareTo("") != 0) {
				aux.setDesciere(newProduct.getDesciere());
			}
			aux.setPret(aux.getPret() + pret);
			aux.setCantitate(aux.getCantitate() + cantitate);
		} else if (policy == WarehousePolicy.NEW_PRODUCT) {
			newProduct.setId(nexId++);
			productTree.addElement(newProduct);
		}
	}

	/**
	 * Cauta produsele care au o cheie specificata ca parametru si returneaza un
	 * ArrayList de produse gasite (un sir de caractere care sa se gaseasca in
	 * numele produsului).
	 * 
	 * @param ch
	 *            cheia dupa care se face cautarea
	 * @return un ArrayList de produse
	 */
	public ArrayList<Product> cauta(String ch) {
		ArrayList<Product> aux = new ArrayList<Product>();

		for (initNext(); hasNext(); nextProduct()) {
			if (getCurrentProduct().getNume().toLowerCase()
					.contains(ch.toLowerCase())) {
				aux.add(getCurrentProduct());
			}
		}

		return aux;
	}

	/**
	 * Returneaza produsul care are id-ul specificat.
	 * 
	 * @param id
	 *            id-ul produsului cautat
	 * @return produsul cu id-ul cerut
	 */
	public Product getProduct(int id) {

		for (initNext(); hasNext(); nextProduct()) {
			if (getCurrentProduct().getId() == id) {
				return getCurrentProduct();
			}
		}

		return null;
	}

	/**
	 * Returneaza un ArrayList cu toate produsele stocate.
	 * 
	 * @return un ArrayList cu toate produsele
	 */
	public ArrayList<Product> toArrayList() {
		return productTree.toArrayList();
	}

	/**
	 * Returneaza indexul unui produs.
	 * 
	 * @param product
	 *            produsul la care se doreste aflarea indexului.
	 * @return indexul produsului.
	 */
	public int getIndexOf(Product product) {
		return productTree.getIndexOf(product);
	}

	/**
	 * Sterge un produs.
	 * 
	 * @param removeProduct
	 *            produsul care trebuie sters
	 */
	public void removeProduct(Product removeProduct) {
		productTree.deleteElement(removeProduct);
	}

	/**
	 * Returneaza daca exista (este stocat) sau nu un anumit produs.
	 * 
	 * @param product
	 *            produsul care se doreste testat
	 * @return true daca exista
	 */
	public boolean existProduct(Product product) {
		return productTree.exist(product);
	}

	/**
	 * Verifica daca metoda nextProduct() mai are elemente nevizitate. Aceasta
	 * metoda este functioneaza impreuna cu : getCurrentProduct(),
	 * nextProduct(), initNext().
	 * 
	 * @return true daca mai sunt elemente nevizitate
	 */
	public boolean hasNext() {
		return productTree.hasNext();
	}

	/**
	 * Returneaza elementul curent. Aceasta metoda este functioneaza impreuna cu
	 * : hasNext(), nextProduct(), initNext().
	 * 
	 * @return elementul curent
	 */
	public Product getCurrentProduct() {
		return productTree.getCurrentElement();
	}

	/**
	 * Trece la urmatorul element nevizitat. Aceasta metoda este functioneaza
	 * impreuna cu : hasNext(), getCurrentProduct(), initNext().
	 */
	public void nextProduct() {
		productTree.nextElement();
	}

	/**
	 * Initializeaza elementul curent la cel mai mic sau cel mai mare element in
	 * functie de comparator si de politica de ordonare a elemntelor. Aceasta
	 * metoda este functioneaza impreuna cu : hasNext(), getCurrentProduct(),
	 * nextProduct().
	 */
	public void initNext() {
		productTree.initNext();
	}

	/**
	 * Returneaza numarul de produse stocate.
	 * 
	 * @return numarul de produse stocate
	 */
	public int getSize() {
		return productTree.getSize();
	}
}
