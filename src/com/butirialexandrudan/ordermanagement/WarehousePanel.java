package com.butirialexandrudan.ordermanagement;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class WarehousePanel extends JPanel {

	private GridBagConstraints gbc;
	private JPanel pnlAddProduct;
	private JPanel pnlCautaProduct;
	private JPanel pnlEditProduct;
	private JLabel lblNume = new JLabel("Nume:");
	private JLabel lblCantitate = new JLabel("Cantitate:");
	private JLabel lblPret = new JLabel("Pret:");
	private JLabel lblCategorie = new JLabel("Categorie:");
	private JLabel lblDescriere = new JLabel("Descriere:");
	private JLabel lblEditId = new JLabel("Id produs:");
	private JLabel lblEditNume = new JLabel("Nume:");
	private JLabel lblEditCantitate = new JLabel("Cantitate:");
	private JLabel lblEditPret = new JLabel("Pret:");
	private JLabel lblEditCategorie = new JLabel("Categorie:");
	private JLabel lblEditDescriere = new JLabel("Descriere:");
	private JLabel lblCauta = new JLabel("Cauta:");
	private JTextField tfCantitate = new JTextField();
	private JTextField tfPret = new JTextField();
	private JTextField tfNume = new JTextField();
	private JTextField tfCategorie = new JTextField();
	private JTextField tfDescriere = new JTextField();
	private JTextField tfEditId = new JTextField();
	private JTextField tfEditCantitate = new JTextField();
	private JTextField tfEditPret = new JTextField();
	private JTextField tfEditNume = new JTextField();
	private JTextField tfEditCategorie = new JTextField();
	private JTextField tfEditDescriere = new JTextField();
	private JTextField tfCauta = new JTextField();
	private JButton btnNou = new JButton("Adauga articol nou!");
	private JButton btnCauta = new JButton("Cauta!");
	private JButton btnModifica = new JButton("Actualizeaza articolul!");
	private JButton btnSterge = new JButton("Sterge articolul!");
	private MyTable myTable;
	private JTable table;
	private JScrollPane scrollPane;

	/**
	 * Create the panel.
	 * 
	 * @param warehouse
	 *            lista cu produse
	 */
	public WarehousePanel(Warehouse warehouse) {
		gbc = new GridBagConstraints();
		pnlAddProduct = new JPanel();
		pnlCautaProduct = new JPanel();
		pnlEditProduct = new JPanel();
		myTable = new MyTable(Product.class, AddThis.class,
				new ArrayList<Object>(warehouse.toArrayList()));
		table = new JTable(myTable);
		scrollPane = new JScrollPane(table);

		this.setLayout(new GridBagLayout());
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;

		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(pnlAddProduct, gbc);
		initAddProductComponent(pnlAddProduct);

		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(pnlCautaProduct, gbc);
		initCautaProductComponent(pnlCautaProduct);

		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(pnlEditProduct, gbc);
		initEditProductComponent(pnlEditProduct);

		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.2;
		gbc.weighty = 0.2;
		gbc.gridheight = 3;
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		this.add(scrollPane, gbc);
	}

	private void initAddProductComponent(JPanel panel) {
		panel.setLayout(new GridBagLayout());
		panel.setBorder(BorderFactory.createTitledBorder(null, "Adauga produs",
				TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, null));

		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.HORIZONTAL;

		gbc.ipadx = 10;

		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(lblPret, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(lblNume, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		panel.add(lblCantitate, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		panel.add(lblCategorie, gbc);

		gbc.gridx = 0;
		gbc.gridy = 4;
		panel.add(lblDescriere, gbc);

		gbc.ipadx = 200;

		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(tfPret, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(tfNume, gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		panel.add(tfCantitate, gbc);

		gbc.gridx = 1;
		gbc.gridy = 3;
		panel.add(tfCategorie, gbc);

		gbc.gridx = 1;
		gbc.gridy = 4;
		panel.add(tfDescriere, gbc);

		gbc.ipadx = 10;
		gbc.gridwidth = 2;

		gbc.gridx = 0;
		gbc.gridy = 5;
		panel.add(btnNou, gbc);
	}

	private void initCautaProductComponent(JPanel panel) {
		panel.setLayout(new GridBagLayout());
		panel.setBorder(BorderFactory.createTitledBorder(null, "Cauta produs",
				TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, null));

		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipadx = 1;

		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(lblCauta, gbc);

		gbc.ipadx = 200;

		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(tfCauta, gbc);

		gbc.ipadx = 1;

		gbc.gridx = 2;
		gbc.gridy = 0;
		panel.add(btnCauta, gbc);
	}

	private void initEditProductComponent(JPanel panel) {
		panel.setLayout(new GridBagLayout());
		panel.setBorder(BorderFactory.createTitledBorder(null,
				"Modifica produs", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, null));

		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.HORIZONTAL;

		gbc.ipadx = 10;

		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(lblEditId, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(lblEditPret, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		panel.add(lblEditNume, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		panel.add(lblEditCantitate, gbc);

		gbc.gridx = 0;
		gbc.gridy = 4;
		panel.add(lblEditCategorie, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		panel.add(lblEditDescriere, gbc);

		gbc.ipadx = 200;

		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(tfEditId, gbc);
		tfEditId.setBackground(Color.RED);

		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(tfEditPret, gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		panel.add(tfEditNume, gbc);

		gbc.gridx = 1;
		gbc.gridy = 3;
		panel.add(tfEditCantitate, gbc);

		gbc.gridx = 1;
		gbc.gridy = 4;
		panel.add(tfEditCategorie, gbc);

		gbc.gridx = 1;
		gbc.gridy = 5;
		panel.add(tfEditDescriere, gbc);

		gbc.ipadx = 10;

		gbc.gridx = 0;
		gbc.gridy = 6;
		panel.add(btnModifica, gbc);

		gbc.gridx = 1;
		gbc.gridy = 6;
		panel.add(btnSterge, gbc);
	}

	/**
	 * Returneaza butonul pentreu adaugarea unui nou produs.
	 * 
	 * @return butonul pentreu adaugarea unui nou produs
	 */
	public JButton getBtnNou() {
		return btnNou;
	}

	/**
	 * Returneaza butonul de cautare produs.
	 * 
	 * @return butonul de cautare produs
	 */
	public JButton getBtnCauta() {
		return btnCauta;
	}

	/**
	 * Returneaza TextField-ul in care este specificata cantitatea (pentru
	 * adaugarea unui produs nou).
	 * 
	 * @return TextField-ul in care este specificata cantitatea
	 */
	public JTextField getTfCantitate() {
		return tfCantitate;
	}

	/**
	 * Returneaza TextField-ul in care este specificata pretul produsului
	 * (pentru adaugarea unui produs nou).
	 * 
	 * @return TextField-ul in care este specificata pretul
	 */
	public JTextField getTfPret() {
		return tfPret;
	}

	/**
	 * Returneaza TextField-ul in care este specificata numele produsului
	 * (pentru adaugarea unui produs nou).
	 * 
	 * @return TextField-ul in care este specificata numele
	 */
	public JTextField getTfNume() {
		return tfNume;
	}

	/**
	 * Returneaza TextField-ul in care este specificata categoria produsului
	 * (pentru adaugarea unui produs nou).
	 * 
	 * @return TextField-ul in care este specificata categoria
	 */
	public JTextField getTfCategorie() {
		return tfCategorie;
	}

	/**
	 * Returneaza TextField-ul in care este specificata descrierea produsului
	 * (pentru adaugarea unui produs nou).
	 * 
	 * @return TextField-ul in care este specificata descrierea
	 */
	public JTextField getTfDescriere() {
		return tfDescriere;
	}

	/**
	 * Returneaza TextField-ul in care se scrie numele produsului cautat
	 * (tfCauta).
	 * 
	 * @return TextField-ul in care se scrie numele produsului cautat
	 */
	public JTextField getTfCauta() {
		return tfCauta;
	}

	/**
	 * Returneaza TextField-ul in care este specificata cantitatea (pentru
	 * modificarea unui produs existent).
	 * 
	 * @return TextField-ul in care este specificata cantitatea
	 */
	public JTextField getTfEditCantitate() {
		return tfEditCantitate;
	}

	/**
	 * Returneaza TextField-ul in care este specificata pretul (pentru
	 * modificarea unui produs existent).
	 * 
	 * @return TextField-ul in care este specificata pretul
	 */
	public JTextField getTfEditPret() {
		return tfEditPret;
	}

	/**
	 * Returneaza TextField-ul in care este specificata numele (pentru
	 * modificarea unui produs existent).
	 * 
	 * @return TextField-ul in care este specificata numele
	 */
	public JTextField getTfEditNume() {
		return tfEditNume;
	}

	/**
	 * Returneaza TextField-ul in care este specificata categoria (pentru
	 * modificarea unui produs existent).
	 * 
	 * @return TextField-ul in care este specificata categoria
	 */
	public JTextField getTfEditCategorie() {
		return tfEditCategorie;
	}

	/**
	 * Returneaza TextField-ul in care este specificata descrierea (pentru
	 * modificarea unui produs existent).
	 * 
	 * @return TextField-ul in care este specificata descrierea
	 */
	public JTextField getTfEditDescriere() {
		return tfEditDescriere;
	}

	/**
	 * Returneaza TextField-ul in care este specificata id-ul produsului care se
	 * doreste modificat.
	 * 
	 * @return TextField-ul in care este specificata id-ul produsului care se
	 *         doreste modificat
	 */
	public JTextField getTfEditId() {
		return tfEditId;
	}

	/**
	 * Returnneaza butonul pentru modificarea produsului.
	 * 
	 * @return butonul pentru modificarea produs
	 */
	public JButton getBtnModifica() {
		return btnModifica;
	}

	/**
	 * Returneaza butonul pentru stergerea unui produs.
	 * 
	 * @return butonul pentru stergerea unui produs
	 */
	public JButton getBtnSterge() {
		return btnSterge;
	}

	/**
	 * Returneaza obiectul care retine elementele care sunt puse in tabel
	 * (myTable).
	 * 
	 * @return obiectul care retine elementele care sunt puse in tabel
	 */
	public MyTable getMyTable() {
		return myTable;
	}

	/**
	 * Returneaza tabelul in care sunt afisate produsele (table).
	 * 
	 * @return tabelul in care sunt afisate produsele
	 */
	public JTable getTable() {
		return table;
	}
}
