package com.butirialexandrudan.ordermanagement;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JTable;

public class CustomerControl {
	private CustomerPanel customerPanel;
	private CustomerList customerList;
	private MyTable myTable;
	private JTable table;

	/**
	 * Creaza obiectul.
	 * 
	 * @param newMainFrame
	 *            fereastrea principala
	 * @param newCustomerList
	 *            lista cu clientii
	 */
	public CustomerControl(MainFrame newMainFrame, CustomerList newCustomerList) {
		this.customerPanel = newMainFrame.getMainPanel().getCustomerPanel();
		this.customerList = newCustomerList;
		this.myTable = newMainFrame.getMainPanel().getCustomerPanel()
				.getMyTable();
		this.table = newMainFrame.getMainPanel().getCustomerPanel().getTable();

		customerPanel.getBtnAdd().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg) {

				customerList.addCustomer(new Customer());
				myTable.addObject(customerList.getLastCustomer());
				myTable.setCellEditable(myTable.getRowCount() - 1, 1, true);
				myTable.setCellEditable(myTable.getRowCount() - 1, 2, true);
				myTable.setCellEditable(myTable.getRowCount() - 1, 3, true);

			}

		});

		customerPanel.getBtnDelete().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg) {
				int aux[] = table.getSelectedRows();

				for (int i = aux.length - 1; i >= 0; --i) {
					customerList.removeCustomer(Integer.parseInt(myTable
							.getValueAt(aux[i], 0).toString()));
					myTable.removeObject(aux[i]);
				}

			}

		});

		customerPanel.getBtnCauta().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg) {

				myTable.addNewTable(new ArrayList<Object>(customerList
						.cauta(customerPanel.getTfCauta().getText())));

			}
		});

		customerPanel.getTfCauta().addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent arg) {

				if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
					myTable.addNewTable(new ArrayList<Object>(customerList
							.cauta(customerPanel.getTfCauta().getText())));
				}

			}

			public void keyReleased(KeyEvent arg) {

			}

			public void keyTyped(KeyEvent arg) {

			}

		});
	}

}
